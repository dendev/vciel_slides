# Slides projets inscription Vciel
> faite avec [revealjs](https://revealjs.com/)

## Install
```bash 
git clone https://gitlab.com/dendev/vciel_slides
cd vciel_slides
```

## Utilisation 
### avec node
```bash
npm run start
google-chrome http://localhost:8000/
```
### sans node
utilise la version déjà "compilé"
```bash
google-chrome index.html
```
